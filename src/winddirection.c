/*
    File Use:   Source for wind direction sensor
    Managed By: Alycia Hendrickson and Rick Palmer
*/
#include "winddirection.h"
#include "winddirection_local.h"

// Initial setup of the wind direction sensor
void initialize_winddirection()
{

}

// Calculation and accumulation of wind direction
void accumulate_winddirection(unsigned int adcWinddirection, unsigned intIteration,
                              float fltNorth)
{
    float true_direction;
    float conversion = 0.351967;                                     // conversion factor
    float degree = adcWinddirection >> 6;                            // shifts raw data from DMA to correct format
    degree = degree * conversion;                                    // converts from bits to degrees

    true_direction = degree + fltNorth;                              // adds offset due to position of sensor

    if (true_direction > 360)                                        // keeps the data range from 0 to 360 degrees
        true_direction -= 360;

    accumDirection += true_direction;                                // accumulates final wind direction measurements
}

// Get noise averaged wind direction value
unsigned int get_winddirection()
{
    float ave_direction = accumDirection / 20;                      // averages 20 data measurements.
    unsigned int rounded_direction = doTensRounding(ave_direction); // rounds data to the nearest ten degrees
    accumDirection = 0;

    if (rounded_direction > 360)                                    // keeps the data range from 0 to 360 degrees
        rounded_direction -= 360;

    return rounded_direction;
}

//Rounds the wind direction data to the nearest 10s place value
unsigned int doTensRounding(float data)
{
    unsigned int rounded;
    float x = data / 10;
    unsigned int tens = (int) x;
    float ones = x - tens;                                          // extracts the ones place value

    if (ones < 0.5)                                                 // if ones place value is less than 5, then rounds tens place value down
        rounded = tens * 10;
    else                                                            // if ones place value is greater or equal to 5, then rounds tens place value up
        rounded = tens * 10 + 10;

    return rounded;
}

//******************************************************
//when all registers pass the self test normal compass operation
//******************************************************
float get_north()
{
    //variable declaration begins here
    float x_total = 0; //These variables give the 16-bit measurement of the directional field strength in mGauss
    float y_total = 0;
    float z_total = 0; 
    signed int x_test = 0;   
    signed int y_test = 0;   
    signed int z_test = 0;
    signed int A_value = 0;
    signed int B_value = 0;
    signed int Mode_value = 0;
    float pi = 3.141593;
    float north = 0;
    signed char x_high = 0;   //These are variables for the number representing the 
    signed char x_low = 0x00;    //strength of each magnetic field in each vector direction
    signed char y_high = 0x00;   //They are 16-bit numbers with high and low bytes     
    signed char y_low = 0;           
    signed char z_high = 0;
    signed char z_low = 0;
    signed char x_h = x_high;   //These are variables for the number representing the 
    signed char x_l = x_low;    //strength of each magnetic field in each vector direction
    signed char y_h = y_high;   //They are 16-bit numbers with high and low bytes     
    signed char y_l = y_low;           
    signed char z_h = z_high;
    signed char z_l = z_low;

    while(1)
    {
        //****************************
        //Set A Configuration Register
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x00;
        delay();
        I2CDATA = 0x70;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        //Set B Configuration Register
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x01;
        delay();
        I2CDATA = 0xA0;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        //Set Mode Register
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x02;
        delay();
        I2CDATA = 0x00;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        //Register Check
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x00;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        A_value = I2CDATA;
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x01;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        B_value = I2CDATA;

        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x02;
        delay();
        I2CCTL= 0xA0;
        delay();
        //****************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        Mode_value = I2CDATA;
        //****************************

        while ((I2CDATA & 0x01) != 0x01)
        {
            I2CCTL = 0x80;
            I2CCTL = 0x88;
            I2CDATA = 0x3C;
            I2CCTL = 0xC0;
            delay();
            I2CDATA = 0x09;
            delay();
            I2CCTL= 0xA0;
            delay();
            I2CCTL = 0x80;
            I2CCTL = 0x88;
            I2CDATA = 0X3D;
            I2CCTL = 0xC0;
            delay();
            I2CCTL = 0x82;
            delay();
            I2CCTL= 0xA0;
            delay();
        }
        //************************
        //grabbing x axis data and storing it in variables
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x03;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        x_high = 0;
        x_high = I2CDATA;
        //************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x04;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        x_low = 0;
        x_low = I2CDATA;
        //************************
        //grabbing z axis data and storing in variables
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x05;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        z_high = 0;
        z_high = I2CDATA;
        //************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x06;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        z_low = 0;
        z_low = I2CDATA;
        //************************
        //grabbing y axis data and storing in variables
        //************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x07;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        y_high = 0;
        y_high = I2CDATA;
        //************************
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0x3C;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CDATA = 0x06;
        delay();
        I2CCTL= 0xA0;
        delay();
        I2CCTL = 0x80;
        I2CCTL = 0x88;
        I2CDATA = 0X3D;
        delay();
        I2CCTL = 0xC0;
        delay();
        I2CCTL = 0x82;
        delay();
        I2CCTL= 0xA0;
        delay();
        y_low = 0;
        y_low = I2CDATA;
        //************************
        //setting up
        //************************
        x_test = ((signed int)x_high)<<8;
        x_total =(float)(x_test + (signed int)x_low)*2.56;
        y_test = ((signed int)y_high)<<8;
        y_total =(float)(y_test + (signed int)y_low)*2.56;
        z_test = ((signed int)z_high)<<8;
        z_total =(float)(z_test + (signed int)z_low)*2.56;
        north = atan2(y_total,x_total)*180/pi;
    }

    return north;
}

void delay()
{
    unsigned int tick = 0;
    while (1)
    {
        tick++;
        if (tick>= 65000)
        {
            tick = 0;
            break;
        }
    }
}
