/*
    File Use:   Source for pressure sensor
    Managed By: Johanna Hosanna
*/
#include "pressure.h"
#include "pressure_local.h"

// Initial setup of the pressure sensor
void initialize_pressure()
{

}

// Switch mux positions
void switch_pressure_mux(unsigned char intPosition)
{
    /* Set the mux position according to the Position paramater. 0 is normal, 1 is
       the 3.3V reading, and 2 is the 4V reading.  */

    PAOUT &= 0xFC;                  // position 0 : A0=0 & A1=0

    if (intPosition==1)             // position 1 : A0=1 & A1=0
        PAOUT ^= 0x01;
    else if (intPosition==2)        // position 2 : A0=0 & A1=1
        PAOUT ^= 0x02;
}

// Calibrate pressure sensor
void  calibrate_pressure(unsigned char intMuxPos, unsigned char intIteration, unsigned int adcPressure)
//adcPressure=the raw value
//intIteration=number 1 to 20
{
    /* Do calibration through the mux. For each mux position (1 and 2), accumulate
       the adcValue. */

    float volts;
    unsigned int intTemp;

    intTemp = adcPressure>>6;                   //shifting
    volts = (float)(intTemp)*0.00322634;        //define volts

    if (intMuxPos==1)
        y1accum+=volts;
    else if (intMuxPos==2)
        y2accum+=volts;
}

// Calculate the shift and scale factors
//m and b variables will be called again

void  finalize_pressure_calibration()
{
    /* Calculate fltScale(m) and fltOffset(b) from accumulated values.  */

    float x1, x2, y1, y2;

    y1=y1accum/20;
    y2=y2accum/20;
    y1accum = 0;
    y2accum = 0;
    x1=2.85;
    x2=4.096;
    fltScale=(y2-y1)/(x2-x1);
    fltOffset=y1-fltScale*x1;
}

// Calculation and accumulation of pressure
void accumulate_pressure(unsigned int adcPressure, unsigned int intIteration)
{
    /* Do the pressure calculations here. adcPressure is the raw, unshifted int from the
       DMA. intIteration is the current iteration.  */

    float volts;

    adcPressure = adcPressure>>6;                   //shifting
    volts = (float)(adcPressure)*0.00322634;        //define volts

    y3accum+=volts;
}

// Get noise averaged pressure value
float get_pressure()
{
    /* Returning the final calculated value as an float.  */

    float x3, minoff, fullout, min, max, scale, offset, inHg, ones, hundredths, thousandths, pressure, y3;

    y3=y3accum/20;
    x3=(y3-fltOffset)/fltScale;
    y3accum = 0;

    minoff=0.204;						//x1
    fullout=4.794;						//x2
    min=15;								//y1
    max=115;							//y2
    scale=(max-min)/(fullout-minoff);
    offset=min-scale*minoff;

    inHg=(scale*x3+offset)*0.295299801647; //pressure value in inHg

    //do rounding for 2 decimal values
    ones = (int)(inHg);
    hundredths = (int)(inHg * 100.0);
    thousandths= ((inHg * 100.0)-(float)hundredths)*10.0;
    if (thousandths >= 5)
    {
        hundredths++;
    }

    pressure = hundredths/100;

    return pressure;
}
