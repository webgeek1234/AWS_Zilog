/*
    File Use:   Source for main functionality
    Managed By: Aaron Kling
*/
#include "main.h"

void main ()
{
    // Pointer to the ADC_DMA memory block
    unsigned int *adc_output_ptr = (unsigned int*)adc_memory_block;

    // Misc timer ticks
    unsigned char calc_tick              = 0;
    unsigned int  six_hour_tick          = 0;

    // Weather variables
    unsigned int  intDewpoint            = 0;
    unsigned int  intTemperature         = 0;
    unsigned int  intVisibility          = 0;
    unsigned int  intWindDirection       = 0;
    float         fltNorth               = 0.0;
    unsigned int  intWindSpeed           = 0;
    float         fltPressure            = 0.0;

    // Pressure calibration variable
    unsigned char   pressure_calibration = 0;
    unsigned char   pressure_mux_pos     = 0;
    unsigned int    pressure_wait_tick;

    // Iteration counter
    unsigned char intIteration           = 1;

    // Initialize everything
    initialize();

    // Force a pressure calibration on startup
    pressure_calibration++;
    pressure_mux_pos = 1;
    switch_pressure_mux(1);
    pressure_wait_tick = timer_tick + 1000;
    while (timer_tick < pressure_wait_tick) {}
    ADCCTL = 0xA4;

    while(1)
    {
        // Every second, increment second counter
        if (timer_tick > 1000)
        {
            calc_tick++;
            six_hour_tick++;
            timer_tick = 0;
            increment_ws_tick();
        }

        if (!get_ws_done())
            accumulate_windspeed();

        // Every minute, start a calculation
        if (calc_tick > 60 && !pressure_calibration && !intCalculating)
        {
            calc_tick = 0;
            //fltNorth = get_north();
            ADCCTL = 0xA4;
            intCalculating++;
        }

        if (six_hour_tick > 540000 && !intCalculating)
        {
            six_hour_tick = 0;

            // Every 6 hours, calibrate pressure sensor
            pressure_calibration++;
            pressure_mux_pos = 1;
            switch_pressure_mux(1);
            pressure_wait_tick = timer_tick + 1000;
            while (timer_tick != pressure_wait_tick) {}

            adc_dma_done = 0;
            reset_ws_done();
            ADCCTL = 0xA4;
        }

        // If calibrating pressure
        if (pressure_calibration)
        {
            if (adc_dma_done > 4)
            {
                calibrate_pressure(pressure_mux_pos, intIteration, *adc_output_ptr);
                adc_dma_done = 0;

                if (intIteration == 20 && pressure_mux_pos == 1)
                {
                    intIteration = 1;
                    pressure_mux_pos = 2;
                    switch_pressure_mux(2);
                    pressure_wait_tick = timer_tick + 1000;
                    while (timer_tick != pressure_wait_tick) {}
	                ADCCTL = 0xA4;
                }
                else if (intIteration == 20 && pressure_mux_pos == 2)
                {
                    pressure_calibration = 0;
                    intIteration = 1;
                    pressure_mux_pos = 0;
                    switch_pressure_mux(0);
                    pressure_wait_tick = timer_tick + 1000;
                    while (timer_tick != pressure_wait_tick) {}
                    finalize_pressure_calibration();
                }
                else
                {
                    intIteration++;
                    ADCCTL = 0xA4;
                }
            }
        }
        else if (intCalculating && adc_dma_done > 4 && get_ws_done()) // If the DMA and WS sensor both have new values
        {
            // Calculate the weather values
            accumulate_humidity(*(adc_output_ptr+4), intIteration);
            accumulate_temperature(*(adc_output_ptr+1), intIteration);
            accumulate_visibility(*(adc_output_ptr+2), intIteration);
            accumulate_winddirection(*(adc_output_ptr+3), intIteration, fltNorth);
            accumulate_pressure(*adc_output_ptr, intIteration);

            if (intIteration == 20)
            {
                intTemperature   = get_temperature();
                intDewpoint      = get_dewpoint(intTemperature);
                intVisibility    = get_visibility();
                intWindDirection = get_winddirection();
                intWindSpeed     = get_windspeed();
                fltPressure      = get_pressure();

                intIteration = 1;
                adc_dma_done = 0;
                reset_ws_done();
                intCalculating = 0;

                // Send stuff through UART to the RPi
                printf("%d;%d;%d;%d;%d;%f\n",
                       intDewpoint, intTemperature, intVisibility, intWindDirection, intWindSpeed,
                       fltPressure);
            }
            else
            {
                intIteration++;
                adc_dma_done = 0;
                ADCCTL = 0xA4;
            }
        }
    }
}

void initialize()
{
    char strTime[15];
	unsigned int intDummy;

    // Disable Interrupts
    DI();

    // initialize PORTA pins as outputs to sink current
    PAADDR	= 0x02;
    PACTL	= 0xC0;
    PAADDR	= 0x01;
    PACTL   = 0x00;
    PAADDR 	= 0x03;
    PACTL 	= 0x00;
    PAADDR 	= 0x04;
    PACTL 	= 0x00;
    PAADDR 	= 0x05;
    PACTL 	= 0x00;
    PAOUT   = 0xFF;

    // initialize PORTB pins 0-4 for analog input
    PBADDR 	= 0x02;
    PBCTL 	= 0x1F;
    PBADDR	= 0x01;
    PBCTL   = 0xF4;
    PBADDR 	= 0x03;
    PBCTL 	= 0x00;
    PBADDR 	= 0x04;
    PBCTL 	= 0x00;
    PBADDR 	= 0x05;
    PBCTL 	= 0x00;

    // Initialize PORTC pins 0, 1, and 6 as inputs and
    // Pins 2-5 as SPI
    PCADDR  = 0x01;
    PCCTL   = 0x83;
    PCADDR  = 0x02;
    PCCTL   = 0x3C;
    PCADDR  = 0x03;
    PCCTL   = 0x00;
    PCADDR  = 0x04;
    PCCTL   = 0x00;
    PCADDR  = 0x05;
    PCCTL   = 0x00;
    PCOUT  ^= 0x2F;

    // Initialize all PORTD pins as outputs, disabled and not used
    PDADDR  = 0x02;
    PDCTL   = 0x00;
    PDADDR  = 0x01;
    PDCTL   = 0x00;
    PDADDR  = 0x03;
    PDCTL   = 0x00;
    PDADDR  = 0x04;
    PDCTL   = 0x00;
    PDADDR  = 0x05;
    PDCTL   = 0x00;
    PDOUT   = 0xFF;

    // Initialize timer 0 to count and interupt the code every millisecond
    SET_VECTOR(TIMER0, isr_timer0);
    T0CTL1	&= 0x7F;
    T0CTL0	&= 0xEF;
    T0H      = 0x00;
    T0L      = 0x01;
    T0RH     = 0x4E;
    T0RL     = 0x20;
    T0CTL1	 = 0xC1;
    IRQ0ENH |= 0x20;
    IRQ0ENL &= 0xDF;

    // Initialize ADC_DMA for one-shot mode with interrupt
    SET_VECTOR(DMA, isr_dma);
    DMAA_ADDR = (adc_memory_block & 0x0FF0) >> 4;
    DMAACTL   = 0xC4;
    IRQ2ENH  |= 0x10;
    IRQ2ENL  |= 0x10;

    // Initialize UART1 to 57.6 kbaud, 8N1
    init_uart(_UART1,_DEFFREQ,57600UL);
    setstopbits_UART1(STOPBITS_1);
    setparity_UART1(PAR_NOPARITY);
    select_port(_UART1);

    // Call other initilizations
    initialize_humidity();
    initialize_pressure();
    initialize_temperature();
    initialize_visibility();
    initialize_winddirection();
    initialize_windspeed();

    // Enable Interrupts
    EI();
}


// ISR for timer0
#pragma interrupt
void isr_timer0(void)
{
    DI();
    timer_tick++;
    EI();
}

// ISR for adc_dma (sets flag so main knows new values are available)
#pragma interrupt
void isr_dma(void)
{
    DI();
    adc_dma_done++;
    EI();
}
