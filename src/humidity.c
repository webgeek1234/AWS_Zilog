/*
    File Use:   Source for humidity sensor
    Managed By: Josh Mickey
*/
#include "humidity.h"
#include "humidity_local.h"
#include <math.h>

// Initial setup of the humidity sensor
void initialize_humidity()
{

}

// Calculation and accumulation of humidity
void accumulate_humidity(unsigned int adcHumidity, unsigned int intIteration)
{
    // variable declarations begin here
    unsigned int   RH_value = 0;                                       // variable for relative humidity from port
    float   RH_calculated = 0;                                         // variable used for calculations
    float   RH_volt_scaled = 0;                                        // variable for the correct original voltage value
    float   volt_scale = 0.00322634;                                   // 1 digital level= 3Vmax/(2^10)=0.0029296875

    RH_value = adcHumidity;                                            // get the 10 bit ADC output
    RH_value = RH_value >> 6;                                          // slide digital value over six places for least sig. position
    RH_volt_scaled = volt_scale*RH_value;                              // scaled value of the ten bit value to gain original voltage input

    if (RH_volt_scaled == .56)                                         // Zero offset is equal to .958V.

    {
        RH_calculated = 0;
    }
    else
    {
        RH_calculated = (RH_volt_scaled/(3.0)-.16)/(.0062);            // To get relative humidity from the analong input voltage
    }

    RH_value_accumulate = RH_calculated + RH_value_accumulate;
}

// Get noise averaged humidity value
unsigned int get_dewpoint(unsigned int intTemperature)
{
    /* Return the final calculated value as an unsigned int. ACK */

    float RH_calculated = 0;
    float Temperature_celsius = 0;
    float dew_point = 0;

    Temperature_celsius = (intTemperature-32)*(5.0/9.0);
    RH_calculated = RH_value_accumulate/20;
    RH_calculated = RH_calculated/(1.0546-.00216*intTemperature);      // To get true relative humidty with temperature compensation
    dew_point = (pow(RH_calculated/100,1.0/8.0))*(112+0.9*Temperature_celsius)+0.1*Temperature_celsius-112;
    dew_point = (dew_point)*(9.0/5.0)+32;

    RH_value_accumulate = 0;

    return dew_point;
}
