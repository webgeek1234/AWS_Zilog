/*
    File Use:   Source for temperature sensor
    Managed By: Joyce Callanta
*/
#include "temperature.h"
#include "temperature_local.h"

// Initial setup of the temperature sensor
void initialize_temperature()
{

}

// Calculation of temperature
void accumulate_temperature(unsigned int adcTemperature, unsigned int intIteration)
{
    float tempvolts;

    //shift and change the voltage into degrees
    tempvolts = (adcTemperature >> 6)*0.00322634;       //shift over to align the 10 bits of the adc into the proper positions

    //accumulate the readings
    tempaccumulate = tempaccumulate + tempvolts;        //acculumate the temperature readings to accumulate 20 values
}

// Get noise averaged humidity value
unsigned int get_temperature()
{
//get value from calculations

    float tempaverage, tempscale, tempcelsius, tempreal;
    int ones, tens, round, tempfinal;

    tempaverage = (tempaccumulate/20)-1.0;              //divide by 20 to get average value of the 20 readings
    tempaccumulate = 0;

    tempscale = tempaverage * 100.0;                    //divide by scale factor (10mV = 1 degree Celcius)

    //change offset and convert temperature into fahrenheit
    tempcelsius = (tempscale/1.05);                     //scale temperature to actual ambient temperature in celsius
    tempreal = tempcelsius*(9.0/5.0) + 32;              //convert temperature from celsius to fahrenheit

    //do rounding to nearest degree
    ones = (int)(tempreal);
    tens = (int)(tempreal*10.0);
    round = (ones*10.0) - ((float)tens);

    if (round >= 5)
    {
        ones++;                                         //if the tenths place is equal to or greater than 5, the value will be rounded to the next highest degree
    }

    tempfinal = ones;                                   //final temperature value to the nearest degree

    return tempfinal;
}
