/*
    File Use:   Source for wind speed sensor
    Managed By: Wesley Richardson
*/
#include "windspeed.h"
#include "windspeed_local.h"
#include <math.h>

// Initial setup of the wind speed sensor
void initialize_windspeed()
{
    //initializes the PC0 interupt
    SET_VECTOR(C0, isr_portc0); 		//links hardware interrupt for PC0 to interrupt service routine
    IRQ2ENH |= 0x01;					// Set Interrupt Priority Nominal
    IRQ2ENL &= 0xFE;					// Set Interrupt Priority Nominal

}

// Accessor / reset functions
unsigned int get_windspeed()
{
    return intWindSpeed;
}
unsigned int get_ws_done()
{
    return ws_done;
}
void reset_ws_done()
{
    ws_done = 0;
}
void increment_ws_tick()
{
    ws_tick++;
}

void accumulate_windspeed()
{
    float tenths;
    unsigned int ones;

    if (ws_tick >= 3)                          //for 3 second interval
    {
        ws_iteration++;                        //Add up each iteration
        ws_accum += 309.0*temp_ws_count/(82944.0*2.0);   //formula for wind speed value in knots
        temp_ws_count = 0;                     //reset edges to 0
        ws_tick = 0;                           //reset timer tick to 0

        if (ws_iteration == 20)                //If 20 iterations have been performed
        {
            //do rounding
            ones = floor(ws_accum/20.0);        //average the 20 values
            tenths= ((ws_accum/20.0) - ones)*10.0;
            if (tenths >= 5)
            {
                ones++;
            }
            intWindSpeed = ones;

            ws_accum = 0;                      //reset the accumlator to 0
            ws_iteration = 0;                  //reset the iterations to 0
            ws_done++;                         //final value
        }
    }
}

// This sets up the function for the isr for PC0
#pragma interrupt
void isr_portc0(void)

{
    if (!ws_done)                              // If windspeed is not done, evaluate.
        temp_ws_count++;                       //Add up the edges
}
