/********************************************************************************

	File Name: visibility.c
	Managed By: John Andrews
    File Use:   Source for visibility sensor
		Project: Aviation Weather Station
		Company: Integrated Imagination (Senior Electrical Engineering Project)
		Organization: Pensacola Christian College
	Date: 04/10/2013

********************************************************************************/

#include "visibility.h"
#include "visibility_local.h"

/************************************************************************************

	Initial setup of the visibility sensor. Initialize the accumulation array

************************************************************************************/
void initialize_visibility()
{
    unsigned int i;
    for (i=0; i<MAX_ACCUM_ARRAY_SIZE; i++)
        TimeAvgMOR_Accum[i] = 0;
}



/************************************************************************************

	Calculation and accumulation of visibility
		The purpose of this function is to calculate an instantaneous value
		for visibility. It converts	the ADC value into an measured current
		difference in uA, then translates it into an irradiance value
	Passed Variables: adcVisibility, intIteration
		adcVisibility is the raw integer value from ADC
		intIteration is the current iteration of function (20 total interations)
	Goal: RecIrradiance -- the received irradiance in mW/cm^2

************************************************************************************/
void accumulate_visibility(unsigned int adcVisibility, unsigned int intIteration)
{
    float RecIrradiance = 0.0f;
    float SignalInputCurrent = 0.0f;
	float vis_volts = 0.0f;

    vis_volts = (adcVisibility>>6)*0.00322634;                                                  //Convert ADC value to Voltage
    SignalInputCurrent = ((vis_volts + (V_OFFSET))-(V_INTERCEPT))/(V_SLOPE);						//Convert voltage Input to Current
    RecIrradiance = pow(((SignalInputCurrent+(BETA_OFFSET))/(45.0-(EE_OFFSET))),(1/0.674847));  //Calculate instantaneous Received Irradiance
    RecIrradiance_Accum += RecIrradiance;                                                       //Accumulate a total of 20 iterations
}

/************************************************************************************

	Get time-averaged visibility value.
		After irradiance has been noise averaged over 20 samples, that average will
		be used as the "current reading," which will be given once per minute to
		this function. This value is used to caluclate the Meterological Observable
		Range (MOR) in whole miles, averaged over the last 10 iterations
	Return: FinalMOR -- the Meteorological Observable Range in miles

************************************************************************************/

unsigned int get_visibility()
{
/***********************************************
		Variable Declarations
***********************************************/

	unsigned int i = 0;
	static int j = 0;
	float UnroundedMOR = 0.0f, ExtinctionCoeff = 0.0f, InstMOR = 0.0f, AvgRecIrradiance, FinalMOR = 0.0f, Beta_ext = 0.0f;
	static float TimeAvgMOR_sum = 0.0f;

/***********************************************
		Visibility Calculations
***********************************************/

	AvgRecIrradiance = (RecIrradiance_Accum/NOISE_AVG_ITERATIONS);								//Determine Irradiance
	RecIrradiance_Accum = 0.0f;
	Beta_ext = AvgRecIrradiance/(EMIT_IRRADIANCE);												//Determine the scatter coefficient
	ExtinctionCoeff = -log(Beta_ext-(K_OFFSET))/(RADIANT_LENGTH);									//Determine the extinction coefficient
	InstMOR = fabs(-((log((CONTRAST)-(MOR_OFFSET2)))/(ExtinctionCoeff))*(5.0/8000.0))*(MOR_OFFSET1);	//Determine the visibility in miles

/***********************************************
		Visibility Averaging
***********************************************/

	TimeAvgMOR_Accum[j] = InstMOR;
	j++;
	if (j>9)
	{
		j=0;
	}
	for (i = 0; i < MAX_ACCUM_ARRAY_SIZE; TimeAvgMOR_sum += TimeAvgMOR_Accum[i++]);		//add together all elements of the array
	if (intTimeAvgIncrement<MAX_ACCUM_ARRAY_SIZE)
	{
		intTimeAvgIncrement++;															//increment this index each time the function is called, capping it at 10
	}
	UnroundedMOR = TimeAvgMOR_sum/intTimeAvgIncrement;									//average the array over the elapsed iterations or 10 iterations
	TimeAvgMOR_sum = 0.0f;

/***********************************************
		Final Value
***********************************************/

	FinalMOR = (unsigned int)floor(UnroundedMOR + 0.5f);								//round result for formatting as an unsigned int
		if (FinalMOR>10)
	{
		FinalMOR = 10;																	//clamp calculated value at 10
	}

	return (unsigned int)FinalMOR;
}
