/*
    File Use:   Header for wind speed sensor local functions and variables
    Managed By: Wesley Richardson
*/
#ifndef __WINDSPEED_LOCAL_H__
#define __WINDSPEED_LOCAL_H__

#include <eZ8.h>

// Current Wind Speed
unsigned int intWindSpeed = 0;

// Flag to indicate if the wind speed sensor has a new value
unsigned char ws_done = 0;

// Wind Speed seconds counter
unsigned char ws_tick = 0;

//Accumulation of edges times equation to get wind speed value in knots
float ws_accum = 0.0;

//20 iterations
unsigned int ws_iteration = 0;

//edges counted (256 edges per revolution)
unsigned int temp_ws_count = 0;

// function prototype for PC0 interrupt
void isr_portc0(void);

#endif // __WINDSPEED_LOCAL_H__
