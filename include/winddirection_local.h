/*
    File Use:   Header for wind direction sensors local functions and variables
    Managed By: Alycia Hendrickson and Rick Palmer
*/
#ifndef __WINDDIRECTION_LOCAL_H__
#define __WINDDIRECTION_LOCAL_H__

#include <eZ8.h>
#include <math.h>

float accumDirection = 0.0;

unsigned int doTensRounding(float data);

void delay();

#endif // __WINDDIRECTION_LOCAL_H__
