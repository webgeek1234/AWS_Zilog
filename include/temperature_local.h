/*
    File Use:   Header for temperature sensors local functions and variables
    Managed By: Joyce Callanta
*/
#ifndef __TEMPERATURE_LOCAL_H__
#define __TEMPERATURE_LOCAL_H__

#include <eZ8.h>

float tempaccumulate = 0; //initialize temperature accumulation to be 0

#endif // __TEMPERATURE_LOCAL_H__
