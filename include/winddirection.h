/*
    File Use:   Function header for wind direction sensor
    Managed By: Aaron Kling
*/
#ifndef __WINDDIRECTION_H__
#define __WINDDIRECTION_H__

// Initial setup of the wind direction sensor
void         initialize_winddirection();

// Calculation and accumulation of wind direction
void         accumulate_winddirection(unsigned int adcWindDirection, unsigned int intIteration,
                                      float fltNorth);

// Get noise averaged wind direction value
unsigned int get_winddirection();

// Get north relative to device
float get_north();

#endif // __WINDDIRECTION_H__
