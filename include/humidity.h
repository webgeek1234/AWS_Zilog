/*
    File Use:   Function header for humidity sensor
    Managed By: Aaron Kling
*/
#ifndef __HUMIDITY_H__
#define __HUMIDITY_H__

// Initial setup of the humidity sensor
void         initialize_humidity();

// Calculation and accumulation of humidity
void         accumulate_humidity(unsigned int adcHumidity, unsigned int intIteration);

// Get calculated dewpoint based on noise averaged humidity value
unsigned int get_dewpoint(unsigned int intTemperature);

#endif // __HUMIDITY_H__
