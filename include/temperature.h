/*
    File Use:   Function header for temperature sensor
    Managed By: Aaron Kling
*/
#ifndef __TEMPERATURE_H__
#define __TEMPERATURE_H__

// Initial setup of the temperature sensor
void         initialize_temperature();

// Calculation and accumulation of temperature
void         accumulate_temperature(unsigned int adcTemperature, unsigned int intIteration);

// Get noise averaged humidity value
unsigned int get_temperature();

#endif // __TEMPERATURE_H__
