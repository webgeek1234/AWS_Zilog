/*
    File Use:   Function header for visibility sensor
    Managed By: Aaron Kling
*/
#ifndef __VISIBILITY_H__
#define __VISIBILITY_H__

// Initial setup of the visibility sensor
void         initialize_visibility();

// Calculation and accumulation of visibility
void         accumulate_visibility(unsigned int adcVisibility, unsigned int intIteration);

// Get noise averaged visibility value
unsigned int get_visibility();

#endif // __VISIBILITY_H__
