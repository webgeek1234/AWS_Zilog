/*
    File Use:   Header for main functionality
    Managed By: Aaron Kling
*/
#ifndef __MAIN_H__
#define __MAIN_H__

// DEFINES
// Location of ADC_DMA memory block
#define adc_memory_block 0x0720

// Main includes
#include <eZ8.h>
#include <sio.h>
#include <stdio.h>

// Includes for each other part
#include "humidity.h"
#include "pressure.h"
#include "temperature.h"
#include "visibility.h"
#include "winddirection.h"
#include "windspeed.h"

// Default user flash optionbits.
FLASH_OPTION1 = 0xFF;
FLASH_OPTION2 = 0xFF;

// VARIABLES
// Flag to indicate if the ADC & DMA are done
unsigned char adc_dma_done = 0;

// Main timer tick
unsigned int timer_tick = 0;

// Visibilty timer tick (needed by interrupt)
unsigned char vis_tick               = 0;

// Warming up or calculation is in progress
unsigned char intCalculating         = 0;

// Functions
// Intial setup
void initialize();

// Timer interrupt
void isr_timer0(void);

// ADC_DMA interrupt
void isr_dma(void);

#endif // __MAIN_H__
