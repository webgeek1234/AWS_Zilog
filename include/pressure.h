/*
    File Use:   Function header for pressure sensor
    Managed By: Aaron Kling
*/
#ifndef __PRESSURE_H__
#define __PRESSURE_H__

// Initial setup of the pressure sensor
void  initialize_pressure();

// Switch mux positions
void  switch_pressure_mux(unsigned char intPosition);

// Calibrate pressure sensor
void  calibrate_pressure(unsigned char intMuxPos, unsigned char intIteration, unsigned int adcPressure);

// Calculate the shift and scale factors
void  finalize_pressure_calibration();

// Calculation and accumulation of pressure
void  accumulate_pressure(unsigned int adcPressure, unsigned int intIteration);

// Get noise averaged pressure value
float get_pressure();

#endif // __PRESSURE_H__
