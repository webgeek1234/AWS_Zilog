/*
    File Use:   Header for visibility sensors local functions and variables
    Managed By: John Andrews
*/
#ifndef __VISIBILITY_LOCAL_H__
#define __VISIBILITY_LOCAL_H__

#include <eZ8.h>
#include <math.h>
#include <string.h>

//Constants gathered from linear trendline of tested transfer function
#define V_SLOPE 0.0451
#define V_INTERCEPT 0.9845

//Constants necessary to adjust for discovered offsets
#define V_OFFSET 1.72
#define K_OFFSET -0.1
#define EE_OFFSET 0.0
#define MOR_OFFSET1 0.45
#define MOR_OFFSET2 -0.02
#define BETA_OFFSET 0.0
#define V_CONVERSION 0.002753906250

//Irradiance inside the sample volume, coming from the LEDs mW/cm^2
#define EMIT_IRRADIANCE 0.9339
#define RADIANT_LENGTH 383.0229763

#define MAX_ACCUM_ARRAY_SIZE 10
#define NOISE_AVG_ITERATIONS 20
#define CONTRAST 0.02		//Minimum percent contrast for object to be considered visible

unsigned int intTimeAvgIncrement = 0;
float TimeAvgMOR_Accum[MAX_ACCUM_ARRAY_SIZE];
float RecIrradiance_Accum = 0.0f;

#endif // __VISIBILITY_LOCAL_H__

