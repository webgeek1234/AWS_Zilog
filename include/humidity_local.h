/*
    File Use:   Header for humidity sensors local functions and variables
    Managed By: Josh Mickey
*/
#ifndef __HUMIDITY_LOCAL_H__
#define __HUMIDITY_LOCAL_H__

#include <eZ8.h>

unsigned int RH_value_accumulate = 0;

#endif // __HUMIDITY_LOCAL_H__
