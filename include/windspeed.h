/*
    File Use:   Function header for wind speed sensor
    Managed By: Aaron Kling
*/
#ifndef __WINDSPEED_H__
#define __WINDSPEED_H__

// Initial setup of the wind speed sensor
void initialize_windspeed();

// Accessor / reset functions
unsigned int get_windspeed();
unsigned int get_ws_done();
void         accumulate_windspeed();
void         reset_ws_done();
void         increment_ws_tick();

#endif // __WINDSPEED_H__
