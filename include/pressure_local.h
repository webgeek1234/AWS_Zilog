/*
    File Use:   Header for pressure sensors local functions and variables
    Managed By: Johanna Hosanna
*/
#ifndef __PRESSURE_LOCAL_H__
#define __PRESSURE_LOCAL_H__

#include <eZ8.h>

float fltScale  = 0.0;
float fltOffset = 0.0;
float y1accum   = 0.0;
float y2accum   = 0.0;
float y3accum   = 0.0;

#endif // __PRESSURE_LOCAL_H__
